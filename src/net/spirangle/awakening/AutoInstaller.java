package net.spirangle.awakening;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;


public class AutoInstaller {

    private static final Logger logger = Logger.getLogger(AutoInstaller.class.getName());

    public static void main(final String[] args) {
        ServerConnection.getInstance().getAvailableMods(args[1]);
        if(ServerConnection.getInstance().install()) {
            ImageIcon icon = new ImageIcon(AutoInstaller.class.getResource("/images/banner_sunrise.png"));
            JPanel panel = new JPanel();
            panel.setLayout(new GridLayout(2,1));
            panel.setBounds(0,0,468,120);
            JLabel l1 = new JLabel(icon);
            l1.setVerticalTextPosition(JButton.TOP);
            l1.setHorizontalTextPosition(JButton.CENTER);
            l1.setBounds(0,0,468,60);
            JLabel l2 = new JLabel("Your client mods have been updated, please restart your Wurm Unlimited client.");
            l2.setVerticalTextPosition(JButton.TOP);
            l2.setHorizontalTextPosition(JButton.CENTER);
            panel.add(l1);
            panel.add(l2);
            JOptionPane.showMessageDialog(null,panel,"Client Mod Update",JOptionPane.PLAIN_MESSAGE);
        }
    }
}
