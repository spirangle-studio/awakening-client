package net.spirangle.awakening;

import org.gotti.wurmunlimited.modloader.interfaces.Configurable;
import org.gotti.wurmunlimited.modloader.interfaces.Initable;
import org.gotti.wurmunlimited.modloader.interfaces.PreInitable;
import org.gotti.wurmunlimited.modloader.interfaces.WurmClientMod;

import java.util.Properties;
import java.util.logging.Logger;


public class AwakeningClientMod implements WurmClientMod, Configurable, PreInitable, Initable {

    private static final Logger logger = Logger.getLogger(AwakeningClientMod.class.getName());

    public static boolean verbose = false;
    public static boolean debug = false;

    //   public native void sayHello();

    //   private List<ModInfo> mods;

    @Override
    public void configure(Properties properties) {
        String modsDir = properties.getProperty("forgeModsPath","mods");
        logger.info("modsDir = "+modsDir);
        Config.getInstance().configure(properties);
        ServerConnection.getInstance().getAvailableMods(modsDir);
    }

    @Override
    public void preInit() {
        //      System.load(new File("mods/awakening/nativelibs","libawa.so").getAbsolutePath());

/*      try {
//         mods = getMods(Paths.get("mods"));
      } catch(IOException|NoSuchAlgorithmException e) {
         throw new RuntimeException("Error loading mod info: "+e.getMessage(),e);
      }*/
        //      for(ModInfo mod : mods) {
        //         AwakeningClientMod.log("Mod: "+mod.getName()+" [properties: "+mod.getPropertiesPath()+", jar: "+mod.getJarPath()+", jarSha1: "+mod.getJarSHA1()+"]");
        //      }
        CodeInjections.preInit();

        //      sayHello();
    }

    @Override
    public void init() {
        ServerConnection.getInstance().init();

        //      world = ModClient.getWorld();
        //      world.getServerConnection().sendmessage5(":Awaclient","Testing...");
/*      HookManager.getInstance().registerHook("com.wurmonline.client.game.World","setServerInformation",(String)null,(InvocationHandlerFactory)new InvocationHandlerFactory() {
         public InvocationHandler createInvocationHandler() {
            return new InvocationHandler() {
               @Override
               public Object invoke(final Object proxy,final Method method,final Object[] args) throws Throwable {
                  final Object result = method.invoke(proxy,args);
                  try {
                     world = (World)proxy;
                     for(ModInfo mod : mods) {
                        world.getServerConnection().sendmessage5(":Awaclient","#clientmod "+mod.name+":"+mod.jarSha1);
                     }
                  }
                  catch (Exception e) {
                     AwakeningClientMod.warning(e.getMessage(),e);
                  }
                  return result;
               }
            };
         }
      });*/
    }
}
