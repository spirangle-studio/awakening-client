package net.spirangle.awakening;

import org.gotti.wurmunlimited.modloader.interfaces.Configurable;

import java.util.Properties;
import java.util.logging.Logger;


public class Config implements Configurable {

    private static final Logger logger = Logger.getLogger(Config.class.getName());

    public static boolean autoUpdate = false;
    public static boolean useWindowedFullscreenSizeAndPosition = false;
    public static boolean showExtraTooltips = true;
    public static boolean autoSaveToolBelt = true;

    private static Config instance = null;

    public static Config getInstance() {
        if(instance==null) instance = new Config();
        return instance;
    }

    private Config() {
    }

    public void configure(Properties properties) {
        autoUpdate = Boolean.parseBoolean(properties.getProperty("autoUpdate","true"));
        useWindowedFullscreenSizeAndPosition = Boolean.parseBoolean(properties.getProperty("useWindowedFullscreenSizeAndPosition","false"));
        showExtraTooltips = Boolean.parseBoolean(properties.getProperty("showExtraTooltips","true"));
        autoSaveToolBelt = Boolean.parseBoolean(properties.getProperty("autoSaveToolBelt","true"));
    }
}
