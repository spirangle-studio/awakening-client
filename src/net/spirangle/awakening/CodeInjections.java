package net.spirangle.awakening;

import javassist.CannotCompileException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import net.spirangle.awakening.util.Syringe;

import java.util.logging.Logger;


public class CodeInjections {

    private static final Logger logger = Logger.getLogger(CodeInjections.class.getName());

    public static void preInit() {
        /* World: */
        final Syringe world = Syringe.getClass("com.wurmonline.client.game.World");
        world.insertAfter("setServerInformation","net.spirangle.awakening.ServerConnection.handShake(this);",null);

        /* SimpleServerConnectionClass: */
        final Syringe sscc = Syringe.getClass("com.wurmonline.client.comm.SimpleServerConnectionClass");
        sscc.instrument("reallyHandle",new ExprEditor() {
            int i = 0;

            public void edit(MethodCall mc) throws CannotCompileException {
                if(mc.getMethodName().equals("get")) {
                    if(i==0) {
                        mc.replace("$_ = $proceed($$);if($_==-101) {net.spirangle.awakening.ServerConnection.handlePacket(bb);return;}");
                        logger.info("SimpleServerConnectionClass: Handle custom server communication.");
                    }
                    ++i;
                }
            }
        });

        /* LwjglClient: */
        final Syringe lwjglc = Syringe.getClass("com.wurmonline.client.LwjglClient");
        //      lwjglc.insertBefore("initFullscreen","if(!com.wurmonline.client.options.Options.screenSettings.maximized) {\n"+
        //                                           "   com.wurmonline.client.options.Options.screenSettings.fullscreen = false;\n"+
        //                                           "   initRegular();\n"+
        //                                           "   return;\n"+
        //                                           "}",
        //                          "Modified window update for fixed sized borderless window");
        //      lwjglc.insertBefore("initFullscreen","if(net.spirangle.awakening.client.ClientWindow.initFullscreen()) return;",
        //                          "Modified window update for fixed sized borderless window");
        //      lwjglc.insertBefore("initFullscreen","if(!net.spirangle.awakening.client.ClientWindow.initFullscreen()) { initRegular();return; }",
        //                          "Modified window update for fixed sized borderless window");
        if(Config.useWindowedFullscreenSizeAndPosition) {
            lwjglc.insertBefore("getWindowedFullscreenSizeAndPosition","{\n"+
                                                                       "   java.awt.Rectangle r = net.spirangle.awakening.client.ClientWindow.getWindowedFullscreenSizeAndPosition();\n"+
                                                                       "   if(r!=null) return r;\n"+
                                                                       "}",
                                "Modified window update for fixed sized borderless window");
        }

        /* TilePicker: */
        final Syringe tp = Syringe.getClass("com.wurmonline.client.renderer.TilePicker");
        tp.setBody("getHoverName","return net.spirangle.awakening.zones.Tiles.tilePickerName($0,$0.world,$0.x,$0.y,$0.section,$0.getDistance());",null);

        /* CaveWallPicker: */
        final Syringe cwp = Syringe.getClass("com.wurmonline.client.renderer.cave.CaveWallPicker");
        cwp.setBody("getHoverName","return net.spirangle.awakening.zones.Tiles.caveWallPickerName($0,$0.world,$0.x,$0.y,$0.name,$0.getDistance());",null);

        /* CreatureCellRenderable: */
        final Syringe ccr = Syringe.getClass("com.wurmonline.client.renderer.cell.CreatureCellRenderable");
        ccr.setBody("getHoverName","return net.spirangle.awakening.creatures.Creatures.creatureCellRenderableName($0);",null);
      /*ccr.setBody("getHoverDescription","{"+
                                        "  net.spirangle.awakening.creatures.Creatures.creatureCellRenderableDescription($1,$0.creature,$0.getModelWrapper(),$0.currentLod);\n" +
                                        "  addTexturepathToDevText($1);\n" +
                                        "}",null);*/

        /* DisplayOption: */
        //      final Syringe dispop = Syringe.getClass("com.wurmonline.client.options.DisplayOption");
        //      dispop.setBody("validate","{}",null);

        if(Config.autoSaveToolBelt) {
            /* ToolBeltComponent: */
            final Syringe tbc = Syringe.getClass("com.wurmonline.client.renderer.gui.ToolBeltComponent");
            tbc.insertAfter("itemDropped","this.toolBelt.saveArrangement(com.wurmonline.client.settings.PlayerData.lastToolBelt);",null);
        }
    }
}
